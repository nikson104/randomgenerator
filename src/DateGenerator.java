
import java.util.Date;
import java.util.Random;

public class DateGenerator implements RandomGenerator<Date> {
    @Override
    public Date execute() {
        return new Date(Math.abs(System.currentTimeMillis() - new Random().nextLong()));
    }
}
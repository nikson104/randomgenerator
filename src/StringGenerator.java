

import java.nio.charset.Charset;
import java.util.Random;

public class StringGenerator implements RandomGenerator <String> {

    @Override
    public String execute() {
        byte[] array = new byte[7];
        String generatedString = new String(array, Charset.forName("UTF-8"));
        return generatedString;
    }
}
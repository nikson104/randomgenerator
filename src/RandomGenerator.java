
public interface RandomGenerator<T> {
    T execute();
}


import java.util.Random;

public class BooleanGenerator implements RandomGenerator<Boolean> {

    @Override
    public Boolean execute() {
        return new Random().nextBoolean();
    }
}